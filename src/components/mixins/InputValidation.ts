/** 
 * File: BaseDialog.vue
 * Author: Angelo Berina
 * Date Created: 06-24-2021
 * Since: v0.0.1
 */

import { Component, Vue, Prop } from 'vue-property-decorator';
import { startCase } from 'lodash-core';
import { Nullable } from '@/types/Nullable';

@Component
export default class InputValidationMixin extends Vue {
  @Prop({ type: Object, default: null })
  public validator!: Nullable<any>;

  @Prop({ type: String, required: true })
  public value!: string;

  @Prop({ type: String, required: true })
  public label!: string;

  public get hasError() {
    const { $dirty, $error } = this.validator;
    return $dirty ? !$error : null;
  }

  public validationError() {
    if (!this.validator || !this.validator.$dirty) { return; }

    if (!this.validator.required) {
      return `${ startCase(this.label) } is required`;
    }
  }
}
