export enum Status {
  pending = 'pending',
  done = 'done'
};

export interface Task {
  date: string;
  description: string;
  status: Status;
}

export interface TaskPayload extends Task {
  id: number;
}