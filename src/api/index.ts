import Dexie from 'dexie';

export const mountDb = () => {
  const dbInstance = new Dexie('tasksDb') as any;

  return dbInstance.version(1).stores({
    tasks: '++id, date, description, status'
  });
}


