import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';
import { mountDb } from './api';

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import './styles/app.scss';

Vue.config.productionTip = false;
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);

Vue.mixin({
  computed: {
    $db: () => {
      return (mountDb() as any).db;
    }
  }
})

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
